import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import { Audio } from 'expo-av';

export default class App extends React.Component {

  async componentDidMount(){
    this.sound = new Audio.Sound();
    const status = {
      shouldPlay: false
    };
    this.sound.loadAsync(require("./assets/music/button-press.mp3"), status, false);
  }

  constructor() {
    super();
    this.state = {
      uri: require("./assets/images/dice1.png")
    }
  }

  playSound = () => {
    this.sound.setPositionAsync(0);
    this.sound.playAsync();
  }

  getRandomValue = () => {
    return Math.floor(Math.random() * 6 ) + 1;
  }

  rollDice = () => {
    const randomNumber = this.getRandomValue();
    this.playSound();

    switch(randomNumber){
      case 1:
        this.setState({uri: require("./assets/images/dice1.png")})
        break;

      case 2:
        this.setState({uri: require("./assets/images/dice2.png")})
        break;

      case 3:
        this.setState({uri: require("./assets/images/dice3.png")})
        break;

      case 4:
        this.setState({uri: require("./assets/images/dice4.png")})
        break;

      case 5:
        this.setState({uri: require("./assets/images/dice5.png")})
        break;

      default:
        this.setState({uri: require("./assets/images/dice6.png")})
        break;

    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Image
          source={this.state.uri}/>
        <TouchableOpacity
          onPress={this.rollDice}
        >
          <Text style={styles.gameButton}>
            Play Game
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F0DF87',
    alignItems: 'center',
    justifyContent: 'center',
  },
  gameButton: {
    marginTop: 35,
    fontSize: 20,
    color: "#FFFFFF",
    fontWeight: "bold",
    borderWidth: 2,
    paddingVertical: 8,
    paddingHorizontal: 40,
    borderRadius: 5,
    borderColor: "#FFFFFF"
  }
});
