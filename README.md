# Dice Roller App
A dice roller app made using React Native and Expo. 

![](DiceRoller.gif)

## Features
- Press the 'Play Game' button and see the dice roll
- Hear a sound effect when the dice rolls

## Running the project
1. `npm install` to install dependencies
2. `expo start` to start the app
